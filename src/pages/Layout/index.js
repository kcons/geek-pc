import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  HomeOutlined,
  DiffOutlined,
  EditOutlined,
  LogoutOutlined,
} from '@ant-design/icons'

import './index.scss'

import { Layout, Menu, theme, Dropdown, Space } from 'antd'
import { Link, useLocation, Outlet, useNavigate } from 'react-router-dom'


import React, { useState, useEffect } from 'react'
import { observer } from 'mobx-react-lite'
import { useStore } from '@/store'

const { Header, Sider, Content } = Layout

const items = [
  {
    label: (
      <a target="_blank" rel="noopener noreferrer" href="https://ant.design/">
        个人中心
      </a>
    ),
    key: '0',
  },
  {
    label: (
      <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
        个人设置
      </a>
    ),
    key: '1',
  },
  {
    type: 'divider',
  },
  {
    label: '退出登录',
    key: '3',
    icon: <LogoutOutlined />
  },
]

const GeekLayout = () => {
  const [collapsed, setCollapsed] = useState(false)
  const { userStore, loginStore } = useStore()
  // 获取用户数据
  useEffect(() => {
    try {
      userStore.getUserInfo()
    } catch { }
  }, [userStore])
  const {
    token: { colorBgContainer }
  } = theme.useToken()

  const location = useLocation()
  const selectedKey = location.pathname

  const navigate = useNavigate()
  const onClick = ({ key }) => {
    if (key === '3') {
      onLogout()
    }
  }
  const onLogout = () => {
    navigate('/login')
    loginStore.loginOut()
  }

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          mode="inline"
          theme="dark"
          style={{ borderRight: 0 }}
          selectedKeys={[selectedKey]}
        >
          <Menu.Item icon={<HomeOutlined />} key="/">
            <Link to="/">数据概览</Link>
          </Menu.Item>
          <Menu.Item icon={<DiffOutlined />} key="/article">
            <Link to="/article">内容管理</Link>
          </Menu.Item>
          <Menu.Item icon={<EditOutlined />} key="/publish">
            <Link to="/publish">发布文章</Link>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
          }}
        >
          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: () => setCollapsed(!collapsed),
          })}
          <div className="user-info">


            <Dropdown
              menu={{
                items,
                onClick
              }}
            >
              <a onClick={(e) => e.preventDefault()}>
                <Space>
                  {userStore.userInfo.name ? 'Serati Ma' : ''}
                </Space>
              </a>
            </Dropdown>
          </div>
        </Header>
        <Content
          style={{
            margin: '24px 16px',
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
          }}
        >
          {/* 二级路由默认页面 */}
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  )
}

export default observer(GeekLayout)