
import { Link } from 'react-router-dom'
import { Breadcrumb, Form, Button, Radio, DatePicker, Select, Popconfirm, Table, Card, Tag, Space, Col, Row } from 'antd'
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'
import 'dayjs/locale/zh-cn'
import locale from 'antd/es/date-picker/locale/zh_CN'
import { useNavigate } from 'react-router-dom'

import img404 from '@/assets/error.png'
import { useEffect, useState } from 'react'

import request from '@/service/request'
import './index.scss'

const { Option } = Select
const { RangePicker } = DatePicker

const Article = () => {
  const columns = [
    {
      title: '封面',
      dataIndex: 'cover',
      width: 120,
      render: cover => {
        return <img src={cover.images[0] || img404} width={80} height={60} alt="" />
      }
    },
    {
      title: '标题',
      dataIndex: 'title',
      width: 220
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: data => <Tag color="green">审核通过</Tag>
    },
    {
      title: '发布时间',
      dataIndex: 'pubdate'
    },
    {
      title: '阅读数',
      dataIndex: 'read_count'
    },
    {
      title: '评论数',
      dataIndex: 'comment_count'
    },
    {
      title: '点赞数',
      dataIndex: 'like_count'
    },
    {
      title: '操作',
      render: data => {
        return (
          <Space size="middle">
            <Button type="primary" shape="circle" icon={<EditOutlined />} onClick={() => goPublic(data)} />
            <Popconfirm
              title="确认删除该条文章吗?"
              onConfirm={() => delArticle(data)}
              okText="确认"
              cancelText="取消"
            >
              <Button
                type="primary"
                danger
                shape="circle"
                icon={<DeleteOutlined />}
              />
            </Popconfirm>
          </Space>
        )
      }
    }
  ]

  // 获取频道列表
  const [channels, setChannels] = useState([])
  useEffect(() => {
    async function fetchChannels () {
      const res = await request.get('/channels')
      setChannels(res.data.channels)
    }
    fetchChannels()
  }, [])

  // 渲染表格数据
  const [article, setArticleList] = useState({
    list: [],
    count: 0
  })

  // 参数管理
  const [params, setParams] = useState({
    page: 1,
    per_page: 10
  })
  // 发送接口请求
  useEffect(() => {
    async function fetchArticleList () {
      const res = await request.get('/mp/articles', params)
      const { results, total_count } = res.data
      setArticleList({
        list: results,
        count: total_count
      })
    }
    fetchArticleList()
  }, [params])

  //查询

  // 筛选功能
  const onSearch = values => {
    const { status, channel_id, date } = values
    // 格式化表单数据
    const _params = {}
    if (status !== -1) {
      _params.status = status
    }
    if (channel_id) {
      _params.channel_id = channel_id
    }
    if (date) {
      _params.begin_pubdate = date[0].format('YYYY-MM-DD')
      _params.end_pubdate = date[1].format('YYYY-MM-DD')
    }
    // 修改params参数 触发接口再次发起
    setParams({
      ...params,
      ..._params
    })
  }

  // 分页
  const pageChange = (page) => {
    // 拿到当前页参数 修改params 引起接口更新
    setParams({
      ...params,
      page
    })
  }

  // 删除记录

  const delArticle = async (data) => {
    await request.delete(`/mp/articles/${data.id}`)
    // 更新列表
    setParams({
      page: 1,
      per_page: 10
    })
  }
  // 编辑跳转
  const navigate = useNavigate()

  const goPublic = (data) => {
    navigate(`/publish?id=${data.id}`)
  }


  return (
    <div>
      <Breadcrumb style={{ marginButtom: 24 }}>
        <Breadcrumb.Item>
          <Link to='/'>首页</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>内容管理</Breadcrumb.Item>
      </Breadcrumb>
      <Form initialValues={{ status: null }} onFinish={onSearch}>
        <Row>
          <Form.Item label="状态" name="status">
            <Radio.Group>
              <Radio value={null}>全部</Radio>
              <Radio value={0}>草稿</Radio>
              <Radio value={1}>待审核</Radio>
              <Radio value={2}>审核通过</Radio>
              <Radio value={3}>审核失败</Radio>
            </Radio.Group>
          </Form.Item>
        </Row>
        <Row>
          <Col>
            <Form.Item label="频道" name="channel_id" style={{ marginRight: 10 }} >
              <Select placeholder="请选择文章频道" style={{ width: 200 }} >
                {channels.map(item => (
                  <Option key={item.id} value={item.id}>
                    {item.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col >
            <Form.Item label="日期" name="date" span={12}>
              {/* 传入locale属性 控制中文显示*/}
              <RangePicker locale={locale} />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item>
              <Button type="primary" htmlType="submit" style={{ marginLeft: 10 }}>
                筛选
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <Card title={`根据筛选条件共查询到 ${article.count} 条结果：`}>
        <Table
          dataSource={article.list}
          columns={columns}
          pagination={{
            position: ['bottomCenter'],
            current: params.page,
            pageSize: params.per_page,
            onChange: pageChange
          }}
        />
      </Card>
    </div >
  )
}
export default Article