import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
// 导入antd样式
import 'antd/dist/reset.css'
// 初始化样式
import './index.scss'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <App />
);

// use Effect 
// 不加依赖项 初始化+ 重新渲染
// 加空数组  初始化执行一次
// 加特定依赖项【】首次执行 + 任意一个变化执行