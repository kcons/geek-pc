import { makeAutoObservable } from "mobx"

import { setToken, getToken, clearToken } from '@/utils/token'

import request from '../service/request'

class LoginStore {
  token = getToken() || ''
  constructor() {
    makeAutoObservable(this)
  }
  login = async ({ mobile, code }) => {
    const res = await request.post('/authorizations', { mobile, code })
    this.token = res.data.token
    setToken(res.data.token)
  }
  // 退出登录
  loginOut = () => {
    this.token = ''
    clearToken()
  }
}

export default LoginStore