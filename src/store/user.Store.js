import { makeAutoObservable } from "mobx"

import request from '../service/request'

class UserStore {
  userInfo = {}
  constructor() {
    makeAutoObservable(this)
  }
  async getUserInfo () {
    const res = await request.get('/user/profile')
    this.userInfo = res.data
  }
}

export default UserStore